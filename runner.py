from scrapy.crawler import CrawlerProcess
from scrapy.settings import Settings

from books.spiders.chitai import ChitaiSpider
from books import settings
from books.enums import ScrapingType


if __name__ == "__main__":
    custom_settings = Settings()
    custom_settings.setmodule(settings)

    # category_or_query = "khudozhestvennaya_literatura-9657"
    # scraping_type = ScrapingType.BOOKS_CATEGORY

    category_or_query = "экономика"
    scraping_type = ScrapingType.QUERY

    process = CrawlerProcess(settings=custom_settings)
    process.crawl(
        ChitaiSpider,
        category_or_query=category_or_query,
        scraping_type=scraping_type
    )

    process.start()
