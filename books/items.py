# Define here the models for your scraped items
#
# See documentation in:
# https://docs.scrapy.org/en/latest/topics/items.html

import scrapy
from scrapy import Selector
from scrapy.loader.processors import MapCompose, Compose, Join, TakeFirst


def clear_string(s):
    return s.strip()


def deduplicate_list(arr):
    return list(set(arr))


def get_joined_text(prop, cls):
    return " ".join(prop.xpath(f"//div[@class='{cls}']//text()").getall()).strip()


def parse_characteristics(arr):
    info = {}
    if arr:
        sel = Selector(text=arr[0])
        properties = sel.xpath("//div[@class='product-prop']")
        for prop in properties:
            key = get_joined_text(prop, "product-prop__title")
            value = get_joined_text(prop, "product-prop__value")
            info[key] = value
    return info


class BooksItem(scrapy.Item):
    url = scrapy.Field(output_processor=TakeFirst())
    title = scrapy.Field(
        input_processor=MapCompose(clear_string),
        output_processor=Join(separator=" ")
    )
    price = scrapy.Field(output_processor=TakeFirst())
    currency = scrapy.Field(output_processor=TakeFirst())
    photo_urls = scrapy.Field(input_processor=Compose(deduplicate_list))
    photo_info = scrapy.Field()
    characteristics = scrapy.Field(
        input_processor=Compose(parse_characteristics)
    )
