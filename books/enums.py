import enum


class ScrapingType(enum.Enum):
    BOOKS_CATEGORY = 1
    QUERY = 2
