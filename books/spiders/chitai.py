import json

import scrapy
from scrapy.http import TextResponse, FormRequest, Request
from scrapy.loader import ItemLoader

from typing import List
from urllib.parse import quote_plus

from books.enums import ScrapingType
from books.items import BooksItem


BASE_URL = "https://www.chitai-gorod.ru/"
AJAX_URL_0 = "https://webapi.chitai-gorod.ru/web/click/recorder/" \
             "?token=123&action=create&" \
             "data%5BrequestUri%5D={}&" \
             "data%5Breferrer%5D={}"
AJAX_URL_1 = "https://search-v2.chitai-gorod.ru/api/v3/search/"
AJAX_URL_2 = "https://webapi.chitai-gorod.ru/web/goods/extension/list/"


def create_url_for_book(url_id):
    return f"https://www.chitai-gorod.ru/catalog/book/{url_id}/"


def create_url_for_scraping(category_or_query: str, scraping_type: ScrapingType, page: int = 1):
    # https://www.chitai-gorod.ru/
    # search/result/?q=%D1%8D%D0%BA%D0%BE%D0%BD%D0%BE%D0%BC%D0%B8%D0%BA%D0%B0
    # &page=2
    #
    # https://www.chitai-gorod.ru/catalog/books/khudozhestvennaya_literatura-9657/
    if scraping_type == ScrapingType.QUERY:
        return f"{BASE_URL}/search/result/?q={quote_plus(category_or_query)}&page={page}"
    elif scraping_type == ScrapingType.BOOKS_CATEGORY:
        return f"https://www.chitai-gorod.ru/catalog/books/{category_or_query}/"
    raise ValueError(
        f"Something went wrong with "
        f"scraping_type = {scraping_type} and "
        f"category_or_query = {category_or_query} during url creating"
    )


def create_form_data_1(query: str, from_num: int = 0, per_page: int = 24):
    return {
        "index": "goods",
        "query": query,
        "type": "common",
        "from": str(from_num),
        "per_page": str(per_page),
        "filters[available]": "false"
    }


# https://stackoverflow.com/questions/39550477/scrapy-send-form-data-with-multiple-options
def create_form_data_2(data: List[str]):
    return {
        "token": "123",
        "action": "read",
        "data[]": data,
    }


def create_headers(query: str, page: int = 1):
    return {
        "origin": "https://www.chitai-gorod.ru",
        "referer": create_url_for_scraping(query, ScrapingType.QUERY, page),
    }


def get_max_page_number_from_site(total, per_page: int = 24):
    return total // per_page


def extract_cookies(cookies_string):
    cookies_arr = cookies_string.split(";")
    cookies = {}
    for s in cookies_arr:
        try:
            k, v = s.split("=")
        except Exception:
            continue
        cookies[k.strip()] = v.strip()
    return cookies


class ChitaiSpider(scrapy.Spider):
    name = 'chitai'
    allowed_domains = ['chitai-gorod.ru']
    start_urls = ['https://www.chitai-gorod.ru/']

    def __init__(self, category_or_query: str, scraping_type: ScrapingType, max_page_number: int = 5):
        super().__init__()
        self.per_page = 24
        self.category_or_query = category_or_query
        self.scraping_type = scraping_type
        # if self.scraping_type == ScrapingType.QUERY:
        #     self.category_or_query = quote_plus(self.category_or_query)
        self.first_pagination_url = create_url_for_scraping(category_or_query, scraping_type)
        self.max_page_number = max_page_number

        self.pagination_callback = None
        if scraping_type == ScrapingType.QUERY:
            self.pagination_callback = self.parse_search_page
        elif scraping_type == ScrapingType.BOOKS_CATEGORY:
            self.pagination_callback = self.parse_books_category_page
        else:
            raise ValueError(
                f"Something went wrong with "
                f"scraping_type = {scraping_type} and "
                f"category_or_query = {category_or_query} during callback choosing"
            )

    def parse_item_page(self, response: TextResponse):
        loader = ItemLoader(item=BooksItem(), response=response)
        loader.add_value("url", response.url)
        loader.add_xpath("characteristics", "//div[@class='product__properties']")
        # loader.add_xpath("price", "//div[@class='product__trade-block']//div[@class='price']//text()")
        loader.add_xpath("price", "//div[@class='product__trade-block']//meta[@itemprop='price']/@content")
        loader.add_xpath("currency", "//div[@class='product__trade-block']//meta[@itemprop='priceCurrency']/@content")
        loader.add_xpath("title", "//div[contains(@class, 'product__main')]//h1//text()")
        loader.add_xpath("photo_urls", "//div[contains(@class, 'product__media')]//img/@data-src")
        yield loader.load_item()

    def parse_books_category_page(self, response: TextResponse, page_number: int = 1):
        item_urls = response.xpath(
            "//div[contains(@class, 'product-card') "
            "and contains(@class, 'info')]/a/@href"
        ).getall()
        for item_url in item_urls:
            yield response.follow(item_url, callback=self.parse_item_page)

        next_url = response.xpath("//a[contains(@id, '_next_page')]/@href").get()
        next_page_number = page_number + 1
        if next_url and next_page_number < self.max_page_number:
            yield response.follow(
                next_url,
                callback=self.pagination_callback,
                cb_kwargs={"page_number": next_page_number},
            )

    def parse_ajax_list(self, response: TextResponse, page_number: int = 1, from_num: int = 0):
        data = {}
        try:
            data = response.json()
        except json.JSONDecodeError:
            print("Something went wrong during json decode when query list.\nStopping...")
            return

        for field in ['result', 'error']:
            if field not in data:
                print(f"There is no field with name = {field} inside data = {data} when query list.\nStopping...")
                return
        if data['error']:
            print(f"There was an error during query list. data = {data}\nStopping...")
            return
        if not data['result']:
            print(f"There is no data in result during query list. data = {data}\nStopping...")
            return

        for datum in data['result'].values():
            item_url = create_url_for_book(datum['url_id'])
            yield response.follow(url=item_url, callback=self.parse_item_page)

        next_page_number = page_number + 1
        if next_page_number < self.max_page_number:
            yield FormRequest(
                AJAX_URL_1,
                method="POST",
                formdata=create_form_data_1(self.category_or_query, from_num),
                headers=create_headers(self.category_or_query, page_number),
                callback=self.parse_ajax_search,
                cb_kwargs={
                    "from_num": from_num,
                    "page_number": next_page_number,
                },
            )

    def parse_ajax_search(self, response: TextResponse, page_number: int = 1, from_num: int = 0):
        data = {}
        try:
            data = response.json()
        except json.JSONDecodeError:
            print("Something went wrong during json decode.\nStopping...")
            return

        for field in ['total', 'ids']:
            if field not in data:
                print(f"There is no field with name = {field} inside data = {data}\nStopping...")
                return
        if data['total'] == 0:
            print(f"Total == 0. data = {data}\nStopping...")
            return
        if not data['ids']:
            print(f"There is empty ids. data = {data}\nStopping...")
            return

        # ?
        site_max_page_number = get_max_page_number_from_site(data['total'], self.per_page)
        if site_max_page_number < self.max_page_number:
            self.max_page_number = site_max_page_number
        from_num = page_number * self.per_page

        ids = list(map(str, data['ids']))

        yield FormRequest(
            AJAX_URL_2,
            method="POST",
            formdata=create_form_data_2(ids),
            headers=create_headers(self.category_or_query, page_number),
            cb_kwargs={
                "from_num": from_num,
                "page_number": page_number,
            },
            callback=self.parse_ajax_list,
        )

    def parse_ajax_from_token_to_actual(self, response: TextResponse, page_number: int = 1):
        # cookies = extract_cookies(response.headers.getlist('Set-Cookie')[0].decode("utf-8"))
        yield FormRequest(
            AJAX_URL_1,
            method="POST",
            formdata=create_form_data_1(self.category_or_query, 0),
            headers=create_headers(self.category_or_query, page_number),
            # cookies=cookies,
            callback=self.parse_ajax_search,
        )

    # https://stackoverflow.com/questions/39550477/scrapy-send-form-data-with-multiple-options
    def parse_search_page(self, response: TextResponse, page_number: int = 1):
        # cookies = extract_cookies(response.headers.getlist('Set-Cookie')[0].decode("utf-8"))
        # yield response.follow(
        #     AJAX_URL_0.format(
        #         quote_plus(response.url.strip("&page=1")),
        #         quote_plus(response.url)
        #     ),
        #     headers=create_headers(self.category_or_query, page_number),
        #     # cookies=cookies,
        #     callback=self.parse_ajax_from_token_to_actual,
        # )
        yield FormRequest(
            AJAX_URL_1,
            method="POST",
            formdata=create_form_data_1(self.category_or_query, 0),
            headers=create_headers(self.category_or_query, page_number),
            # cookies=cookies,
            callback=self.parse_ajax_search,
        )

    def parse(self, response: TextResponse, **kwargs):
        yield response.follow(self.first_pagination_url, callback=self.pagination_callback)
