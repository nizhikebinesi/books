# Define your item pipelines here
#
# Don't forget to add your pipeline to the ITEM_PIPELINES setting
# See: https://docs.scrapy.org/en/latest/topics/item-pipeline.html


# useful for handling different item types with a single interface
from itemadapter import ItemAdapter

import pymongo
import hashlib
from os.path import splitext

from scrapy.http import Request
from scrapy.pipelines.images import ImagesPipeline


# https://docs.scrapy.org/en/latest/topics/item-pipeline.html#write-items-to-mongodb
class BooksPipeline:
    def __init__(self, mongo_uri, mongo_db):
        self.mongo_uri = mongo_uri
        self.mongo_db = mongo_db

    @classmethod
    def from_crawler(cls, crawler):
        return cls(
            mongo_uri=crawler.settings.get('MONGO_URI'),
            mongo_db=crawler.settings.get('MONGO_DATABASE', 'items')
        )

    def open_spider(self, spider):
        self.client = pymongo.MongoClient(self.mongo_uri)
        self.db = self.client[self.mongo_db]

    def close_spider(self, spider):
        self.client.close()

    # some additional functions...

    def process_item(self, item, spider):
        self.db[spider.name].insert_one(ItemAdapter(item).asdict())
        return item


class BooksImagesPipeline(ImagesPipeline):
    def get_media_requests(self, item, info):
        if item['photo_urls']:
            for img_url in item['photo_urls']:
                try:
                    yield Request(img_url)
                except Exception as e:
                    print(e)

    def item_completed(self, results, item, info):
        if results:
            item["photo_info"] = [r[1] for r in results if r[0]]
            del item["photo_urls"]
        return item

    def file_path(self, request, response=None, info=None, *, item=None):
        # https://docs.scrapy.org/en/latest/_modules/scrapy/pipelines/files.html#FilesPipeline.file_path
        path = super().file_path(request, response=response, info=info, item=item)
        # return path.replace("/", f"/{item['title']}/")
        id_from_url = item['url'].strip("/").split("/")[-1]
        return path.replace("/", f"/{id_from_url}/")
